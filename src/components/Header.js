import React, { Component } from 'react';


class Header extends Component {
    render(props) {
        return (
            <div className="header">
                <h1>{ this.props.headerContent }</h1>
            </div>
        );
    }
}

export default Header;