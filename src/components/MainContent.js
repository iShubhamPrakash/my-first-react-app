import React, { Component } from 'react';

class MainContent extends Component{
    render(props) {
        return (
            <div className="content">
                {this.props.contentText}
            </div>
        );
    }
}

export default MainContent;