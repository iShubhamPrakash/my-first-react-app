import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './components/Header.js'
import Header from './components/Header.js';
import MainContent from './components/MainContent.js'


let headers = {
  header1: "What is React?",
  header2: "About this App",
  header3: "What to do next?",
  header4: "Features of this App"
}

let contents = {
  content1: "In computing, React is a JavaScript framework for building user interfaces. It is maintained by Facebook and a community of individual developers and companies. React can be used as a base in the development of single-page or mobile applications.",
  content2: <p>This is a single page application which I have created purely using react. <br/> <br/>
    This will give you a quick demo of how a react application looks on your Phone or Desktop</p>,
  content3: <ul>
    <li>Add this app to your Home Screen by clicking on the browser menu and tapping "Add to Home Screen"</li>
    <br/> <br/>
    <li>Now close the browser and open the app from your Home Screen.</li>
    <br/> <br/>
    <li>You will see that this will launch just like your other android applications.</li>
    <br/> <br/>
    <li>It works even in offline. If you switch off the internet and launch this app, it would still work</li>
  </ul>,
  content4: <ul>
    <li>Complete Standalone Progressive Web App created using React</li>
    <br/> <br/>
    <li>Works even in offline mode (Uses service Worker)</li>
    <br/> <br/>
    <li>No installation needed</li>
  </ul>
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1>WELCOME!</h1>
          <h2> I am Shubham <br /><br /> and <br /></h2>  <h3> (This is my First React App <span role="img" aria-label="An emoji">😍</span>)</h3>
        </header>

        <Header headerContent={headers.header1} />

        <MainContent contentText={contents.content1}/>

        <Header headerContent={headers.header2} />

        <MainContent contentText={contents.content2} />

        <Header headerContent={headers.header3} />

        <MainContent contentText={contents.content3} />

        <Header headerContent={headers.header4} />

        <MainContent contentText={contents.content4} />

      </div>
    );
  }
}

export default App;